# Programming Hadoop with Spark

This project contains examples for using Spark to query data stored in Hadoops HDFS.
It was part of the master course in Software Engineering and Management at Heilbronn University in 2021.

## Usage

The project is intended to run inside the [Hortonworks HDP Sandbox](https://hortonworks.com/products/sandbox).

Examples are based on [The Ultimate Hands-On Hadoop: Tame your Big Data!](https://www.udemy.com/course/the-ultimate-hands-on-hadoop-tame-your-big-data/)

The `makefile` contains some useful commands to get started and setup the environment.

## Sample ratings (MovieLense 25m)

```csv
0,122914,5.0,0,Avengers: Infinity War - Part II (2019)
0,53125,4.4,00,Pirates of the Caribbean: At World's End (2007)
0,45722,4.5,00,Pirates of the Caribbean: Dead Man's Chest (2006)
0,2571,5.0,000,Matrix
0,142488,1.5,0,Spotlight
0,74754,1.0,00,Room
0,7451,1.0,000,Mean Girls (2004)
0,1721,1.0,000,Titanic (1997)
```


# Presentation

## Intro

Goal: show how you can use spark in the context of HADOOP

For this demonstration we downloaded a set of movie ratings.
We use this movie ratings to make some queries against it.

The first dataset, that is also used in the "Ultimate Hands on Hadoop" online course, is the MovieLense 100k dataset.
This contains older movies until mid 1998. We will create a query to find the worst movies.

In the second part we look at what could be done with extensions.
For this we generate movie recomendation based on some ratings.
Here we will use a newer dataset from end of 2019.

Code snippets used here are mostly taken from this Hadoop course and are slidely modified to work in our environment and the newer dataset.

## Environment

For this demonstration we won't use a real HADOOP cluster with several machines.
Instead we use a Virtual Linux Machine that simulates a Hadoop cluster in Docker.

So from our local computer we connect to the VM using SSH, the standard tool to connect to a remote linux terminal.

I set up this editor (VS code) to abstract this away a bit, so we can edit files diretly on the VM and even drag and drop files into the VM.
How this works excatly is not the focus here, but if anyone is interested, I can explain a bit more how it works later.

## What we have prepared

We launced the VM

In VS code you can see that it is connected via SSH.
`maira_dev` is the username on the VM.
We can see the files that are in the home directory on on the left side.


## First example using RDDs

Let's start with the first code example: LowestRatedMovieSpark.py
In this example we are using python, you could also use Java or Scala
(and even C#/F#, R, Julia or Clojure via 3rd party libraries)

Let's have a short look at the input files
`HadoopMaterials/ml-100k/u.data` contains the ratings, tab-seperated
`HadoopMaterials/ml-100k/u.item` contains the movie names, pipe-seperated

Let's look at `LowestRatedPopularMovieSpark.py`

...

### Demo

We have to upload the rating file into HDFS, then submit the script to spark for execution.

```terminal
$ hadoop fs -copyFromLocal ~/HadoopMaterials/ml-100k .
$ spark-submit LowestRatedPopularMovieSpark.py
...
('Amityville 3-D (1983)', 1.1666666666666667)
('Children of the Corn: The Gathering (1996)', 1.3157894736842106)
('Ed (1996)', 1.3333333333333333)
('Best of the Best 3: No Turning Back (1995)', 1.5)
('Bushwhacked (1995)', 1.5714285714285714)
('Body Parts (1991)', 1.6153846153846154)
('Amityville II: The Possession (1982)', 1.6428571428571428)
('Land Before Time III: The Time of the Great Giving (1995) (V)', 1.6666666666666667)
('Bloodsport 2 (1995)', 1.7)
('Lawnmower Man 2: Beyond Cyberspace (1996)', 1.7142857142857142)
```

## Second example: Movie recomendation

In this example we want to show how to use DataFrames and extensions.
The example we use is generating Movie recomendations.

Here we used the newer dataset (Movies until 2019).
We need one volunteer to thik of at least two movies with a good rating and two bad rating. You can write the name and rating in the chat.

Now lets look at the code in `MovieRecommendationsALS25m.py`.
...

### Demo

Again, upload the ratings and submit the script

```
$ hadoop fs -copyFromLocal -f ~/ratings-small.csv .
$ spark-submit MovieRecommendationsALS25m.py
Ratings for user ID 0:
Avengers: Infinity War - Part II (2019) 5.0
Pirates of the Caribbean: At World's End (2007) 4.4
Pirates of the Caribbean: Dead Man's Chest (2006) 4.5
"Matrix 5.0
Spotlight (2015) 1.5
"Room 1.0
Mean Girls (2004) 1.0
Titanic (1997) 1.0

Top 20 recommendations:
(55820, u'No Country for Old Men (2007)', 12.904706954956055)
(57669, u'In Bruges (2008)', 11.287487030029297)
(56782, u'There Will Be Blood (2007)', 10.7456636428833)
(81591, u'Black Swan (2010)', 10.64838695526123)
(69122, u'"Hangover', 9.47704029083252)
(68157, u'Inglourious Basterds (2009)', 9.370169639587402)
(48516, u'"Departed', 9.235871315002441)
(64614, u'Gran Torino (2008)', 9.051487922668457)
(55247, u'Into the Wild (2007)', 8.967508316040039)
(68237, u'Moon (2009)', 8.923774719238281)
(48774, u'Children of Men (2006)', 8.604151725769043)
(63082, u'Slumdog Millionaire (2008)', 8.587040901184082)
(84152, u'Limitless (2011)', 8.406128883361816)
(54286, u'"Bourne Ultimatum', 8.348651885986328)
(6016, u'City of God (Cidade de Deus) (2002)', 8.262065887451172)
(60684, u'Watchmen (2009)', 8.20730972290039)
(70286, u'District 9 (2009)', 8.065604209899902)
(80463, u'"Social Network', 8.014252662658691)
(76251, u'Kick-Ass (2010)', 7.87188720703125)
(79132, u'Inception (2010)', 7.830421447753906)
```

## Ideas for improvement

* Use python charting library to generate visualization of results

* Try prediction with all 25m ratings (`--executor-memory 8G` still causes out of memory exception)
