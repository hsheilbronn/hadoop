import sys

# Load up movie ID -> movie name dictionary


def loadMovieNames():
    movieNames = {}
    with open("HadoopMaterials/ml-100k/u.item") as f:
        for line in f:
            fields = line.split('|')
            movieNames[int(fields[0])] = fields[1].decode('ascii', 'ignore')
    return movieNames


if __name__ == "__main__":

    print("\n----------------------------------------------\n\n")

    # Load up our movie ID -> name dictionary
    movieNames = loadMovieNames()

    print("search movies:")
    line = raw_input("Enter a movie name: ")

    for key in movieNames:
        name = movieNames[key]
        if line in name.lower():
            print(key, movieNames[key])

    print("done: " + line)
