all:
	make spark1 gap
	make spark2 gap
	make dataframe1 gap
	make dataframe2 gap
	make recomendations

upload:
	hadoop fs -copyFromLocal -f ~/HadoopMaterials/ml-100k .

upload25m-full:
	hadoop fs -copyFromLocal -f ~/ml-25m/ratings.csv .

upload25m:
	hadoop fs -copyFromLocal -f ~/ratings-small.csv .

upload-udata:
	hadoop fs -copyFromLocal -f ~/u.data ml-100k

spark1:
	spark-submit LowestRatedMovieSpark.py

spark2:
	spark-submit LowestRatedPopularMovieSpark.py

dataframe1:
	spark-submit LowestRatedMovieDataFrame.py

add-samples25m:
	cat README.md | grep 0,Avengers -A 7 >> ml-25m/ratings.csv

dataframe2:
	spark-submit LowestRatedPopularMovieDataFrame.py

recomendations:
	spark-submit MovieRecommendationsALS.py

recomendations25m:
	spark-submit MovieRecommendationsALS25m.py

clean:
	hadoop fs -rm -R -skipTrash ./ml-100k ratings.csv ratings-small.csv

gap:
	sleep 2
	echo -e "\n\n\n\n-----------------------------\n\n\n\n"

setup:
	sudo yum update
	sudo yum install git python2-pip
	sudo yum install bash-completion bash-completion-extras
	mkdir -p ~/.ssh
	echo ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMEq92vWnF99O5no7E+nnITW0d6HSklFH1kT3GUwLkPf ma@md > .ssh/authorized_keys
	chmod -R go-rwx .ssh
	wget https://dw9ne0o7jcasn.cloudfront.net/hadoop/HadoopMaterials.zip
	unzip HadoopMaterials.zip
	echo "export TERM=xterm-color" >> .bashrc
	ssh-keygen -t ed25519
	cat .ssh/id_ed25519.pub

download2:
	wget https://files.grouplens.org/datasets/movielens/ml-25m.zip
	unzip ml-25m.zip
	rm ml-25m.zip

filter25:
	awk 'NR % 10000 < 100' ml-25m/ratings.csv > ratings-small.csv
	wc -l ratings-small.csv
